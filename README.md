# `smol-ungol`

A smaller, simpler successor to
[`cirith-ungol`](https://codeberg.org/d2718/cirith-ungol), for serving only
static assets, and using the [`smol`](https://docs.rs/smol/latest/smol/#)
async runtime.

When I wrote Cirith Ungol, I was afraid to try anything other than
Tokio. Now that I'm a little more confident, and because it's meant
to be a single-threaded program anyway, I figured I'd give `smol`
a go. This version has fewer dependencies; notably absent are the
massive hyper/tokio/tower ecosystems. They are fantastic works of
engineering, and I use them quite a bit, but

  1. Sticking with `smol` obviously means I can't use Tokio, and
     no Tokio means no hyper.
  2. The introduction of [`async fn` in
     traits](https://blog.rust-lang.org/2023/12/21/async-fn-rpit-in-traits.html)
     means it's _much_ easier to write a service layer system like the
     one provided by tower. Because I have a specific use case in mind,
     it can be much more specialized, smaller, and simpler than tower.

This project is now more-or-less in shape for use. Further work will only
consist of bug fixes, internal implementation clean-up, and minor additional
features.

## Installation

It should just `cargo build --release`, and you can put it in your `$PATH`.

## Use

To set it up (locally or on a VPS) to very simply serve files, create a
configuration file thus:

`config.toml`:
```toml
root = "/path/to/website/root"
```

And launch it

`sudo nohup smol-ungol config.toml &`

This will serve plain HTTP requests on port 80.

### More involved configuration.

If you want to serve secure (HTTPS) requests, you can add

```toml
http_port = 80
https_port = 443
ssl_cert_file = "/path/to/cert.pem"
ssl_key_file = "/path/to/privkey.pem"
```

If you want to serve _only_ secure requests, you can leave out `http_port`.

### Additional options

```toml
run_as = "www-data"
```

In order to bind to the ports everyone expects you to use (80 and 443),
a web server must be run as root. This presents some security problems,
so by default, when running as root, `smol-ungol` will `chroot` itself
to the web root directory and run from there. If you specify a (preferably
unprivileged) user to `run_as`, `smol-ungol` will, instead of `chroot`ing,
attempt to drop privileges and run as the specified user.

```toml
hosts = [
    "yourdomain.org",
    "blog.yourdomain.org",
    "another-subdomain.yourdomain.org"
]
```

Setting the `hosts` option will cause `smol-ungol` to drop any request
that doesn't have one of the specified hosts in the `Host:` header. This
helps secure against people scanning by IP address for vulnerabilities
to exploit.

```toml
blacklist = [
    "31.31.65.36",
    "104.244.42.193",
    "2a03:2880:f175:81:face:b00c:0:25de",
    "2607:f8b0:4002:c00::8b"
]
```

The `blacklist` option can include any number of IPV4 or IPV6 addresses;
requests from any of those addresses will be immediately dropped.

```toml
rate_requests = 10
rate_seconds = 5
```

Together these options throttle requests by IP address. The above options
will cause `smol-ungol` to start responding with 429 to an address if
it sends more than 10 requests in 5 seconds. This effectively limits
requests to 2 per second from any given address, but allows "bursting" of
up to 10 requests at a time.

```toml
prune_period = 1000
```

Request throttling involves some recordkeeping; the above will set
`smol-ungol` to run through and discard expired records every 1000 requests.

```toml
chunk_size = 1_048_576
```

Sets the buffer size used when sending chunked responses. Smaller values
will result in less memory overhead per request, but may necessitate a
greater number of reads and writes.

```toml
log_file = "/home/me/.local/logs/smol_ungol.log"  
```

By default `smol_ungol` logs requests in `requests.log` in the current
directory; this option can change that.

```toml
[compression]
types = ["gzip", "deflate", "bf"]
min_size = 1024
ignore = ["application/gzip", "image/jpeg", "audio/mp3", "application/epub-zip"]
no_default_ignores = true
```

`types` specifies which compression algorithms to use (By default
`smol-ungol` will use all three, picking whichever comes first in any
given request's `Accept-Encoding` header, but you can limit that with
this option if, for some reason, you want to.)

`min_size` sets the minimum file size to compress. Files smaller than this
(in bytes) will be sent uncompressed regardless of `Accept-Encoding`
header values.

`ignore` specifies MIME types that should served uncompressed (generally
this is because they are compressed already, and compressing again for
transfer probably won't net you much). By default, `smol-ungol` will skip
compression for 

```toml
[mime_types]
cda = "application/x-cdf"
epub = "application/epub-zip"
mid = "audio/midi"
midi = "audio/midi"
```

You can see the default MIME type associations that `smol-ungol` uses to
set the `Content-Type` header on served files (as well as make decisions
about compression) in the `src/mime.rs` file. You can use the
`[mime_types]` stanza to alter or augment this.


 * application/x-7z-compressed
 * application/zip
 * audio/mp4
 * audio/mpeg
 * audio/webm
 * image/gif
 * image/jpeg
 * image/png
 * image/webp
 * video/mpeg
 * video/webm

and types specified in `ignore` will augment this list. If you don't want
these default MIME types compressed, set `no_default_ignores` to `true`.

## Project State

  * [x] serve static files
  * [x] autogenerate directory indices
  * [x] configurable
  * [x] TLS
  * [x] outgoing compression !!! (I never managed to get this working for
    `cirith-ungol` using the
    [`tower-http`](https://docs.rs/tower-http/latest/tower_http/)
    compression middleware.)
  * [x] request rate limiting
  * [x] `Host:` header filter (to drop requests with the wrong, or no, `Host`
    header at the surface)
  * [ ] ~~CORS~~ I finally spent a couple of hours implementing a CORS layer,
    then realized most, if not all, requests should be _simple_ requests.
    So it's there, in `src/layer/cors.rs`, but it's dormant for now. 
  * [ ] switch to [`httparse`](https://docs.rs/httparse/latest/httparse/index.html)
    for request parsing, and use some smarter logic to deal with
    partially-parsed requests
  * [x] Use chunked transfer for compressed response bodies, and stream them
    asynchronously, instead of huffing them all the way into RAM and doing
    the encoding there.
  * [x] Allow for configuring additional associations between file extensions
    and MIME `Content-Type`s.

more sopisticated request handling

  * [x] `If-None-Match:`
  * [x] `If-Modified-Since:`
  * [x] `Range:` requests
  * [x] include `If-Range:` header value in checkes for "unmodified" resources
  * [x] `HEAD`
  * [x] `TRACE`
  * [ ] ~~`OPTIONS`~~ CORS is probably not going to be a design goal at this
    point, hence, this is probably off the table.
  
