/*!
Writing responses.
*/

use std::io::Write;

use eyre::WrapErr;
use http::{header, HeaderMap, HeaderValue};
use smallvec::SmallVec;
use smol::{
    io::{AsyncReadExt, AsyncWrite, AsyncWriteExt, BufWriter},
    prelude::*,
};
use tracing::{event, span, Level};

use crate::{body::Body, SuResponse};

pub const DEFAULT_CHUNK_SIZE: usize = 1024 * 1024;

static SPACE: &[u8] = b" ";
pub static COLON: &[u8] = b": ";
pub static CRLF: &[u8] = b"\r\n";
static TERMINATING_CHUNK: &[u8] = b"0\r\n\r\n";

async fn chunk<R, W>(r: &mut R, w: &mut W, chunk_size: usize) -> eyre::Result<()>
where
    R: AsyncRead + Unpin,
    W: AsyncWrite + Unpin,
{
    let mut buff = vec![0u8; chunk_size];
    let mut sizer: SmallVec<[u8; 32]> = SmallVec::new();
    let mut total: usize = 0;

    loop {
        let n = match r
            .read(&mut buff)
            .await
            .wrap_err("error reading from response body")?
        {
            0 => break,
            n => n,
        };
        event!(Level::DEBUG, "chunk() read {} bytes", &n);
        total += n;
        write!(&mut sizer, "{:x}", &n)?;
        w.write_all(&sizer).await?;
        w.write_all(CRLF).await?;
        w.write_all(&buff[..n]).await?;
        w.write_all(CRLF).await?;
    }
    w.write_all(TERMINATING_CHUNK).await?;

    event!(Level::DEBUG, "chunk() wrote {} payload bytes", &total);

    Ok(())
}

async fn write_headers<W>(sink: &mut BufWriter<W>, headers: &HeaderMap) -> eyre::Result<()>
where
    W: AsyncWrite + Unpin,
{
    for (name, val) in headers.iter() {
        sink.write_all(name.as_str().as_bytes()).await?;
        sink.write_all(COLON).await?;
        sink.write_all(val.as_bytes()).await?;
        sink.write_all(CRLF).await?;
        event!(
            Level::DEBUG,
            "    wrote {}: {}",
            name.as_str(),
            &String::from_utf8_lossy(val.as_bytes())
        );
    }

    Ok(())
}

pub async fn blow<W>(w: W, r: SuResponse, chunk_size: usize) -> eyre::Result<()>
where
    W: AsyncWrite + Unpin,
{
    let span = span!(Level::TRACE, "blow([ ... ]) ...");
    let _span = span.enter();

    let mut sink = BufWriter::new(w);

    let mut version: [u8; 8] = [0u8; 8];
    write!(&mut version[..], "{:?}", &r.version())?;

    sink.write_all(&version).await?;
    sink.write_all(SPACE).await?;
    sink.write_all(r.status().as_str().as_bytes()).await?;
    if let Some(reason) = r.status().canonical_reason() {
        sink.write_all(SPACE).await?;
        sink.write_all(reason.as_bytes()).await?;
    }
    sink.write_all(CRLF).await?;
    event!(Level::DEBUG, "wrote first line");

    let (mut parts, body) = r.into_parts();

    let should_chunk = matches!(
        (&body, parts.headers.get(header::CONTENT_LENGTH)),
        (Body::Boxed(_), Some(_))
    );
    if should_chunk {
        parts.headers.insert(
            header::TRANSFER_ENCODING,
            HeaderValue::from_static("chunked"),
        );
    }

    write_headers(&mut sink, &parts.headers).await?;
    sink.write_all(CRLF).await?;

    event!(Level::DEBUG, "headers written");
    event!(Level::DEBUG, "writing body: {:?}", &body);

    match body {
        Body::Empty => { /* don't write any body */ }
        Body::Static(bytes) => sink.write_all(bytes).await?,
        Body::Vec(bytes) => sink.write_all(bytes.as_slice()).await?,
        Body::File(f) => {
            smol::io::copy(f, &mut sink).await?;
        }
        Body::Boxed(mut reader) => {
            if should_chunk {
                chunk(&mut reader, &mut sink, chunk_size).await?
            } else {
                smol::io::copy(reader, &mut sink).await?;
            }
        }
    }

    event!(Level::DEBUG, "body written");
    sink.flush().await?;
    event!(Level::DEBUG, "body written and flushed");

    Ok(())
}
