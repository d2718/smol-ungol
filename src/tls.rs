/*!
Everything needed to generate a TLS connection acceptor from cert and
key files.

`smol-ungol` uses the
[`async_tls`](https://docs.rs/async-tls/latest/async_tls/index.html) crate,
which, as of Feb 2024, is looking for a maintainer. `async_tls` depends on
an older version of `rustls` (0.21), whose API is not compatible with the
current version's.
*/

use std::{fs::File, io::BufReader, path::Path};

use async_tls::TlsAcceptor;
use eyre::{eyre, WrapErr};
use rustls::{Certificate, PrivateKey, ServerConfig};

fn load_certs_and_key(
    cert_path: &Path,
    key_path: &Path,
) -> eyre::Result<(Vec<Certificate>, PrivateKey)> {
    let cert_file = File::open(cert_path)
        .wrap_err_with(|| format!("error opening certificate file {}", cert_path.display()))?;
    let mut cert_rdr = BufReader::new(cert_file);
    let certs = rustls_pemfile::certs(&mut cert_rdr)
        .wrap_err_with(|| format!("error reading certificate file {}", cert_path.display()))?
        .into_iter()
        .map(Certificate)
        .collect::<Vec<_>>();

    let key_file = File::open(key_path)
        .wrap_err_with(|| format!("error opening key file {}", key_path.display()))?;
    let mut key_rdr = BufReader::new(key_file);
    let key = rustls_pemfile::pkcs8_private_keys(&mut key_rdr)
        .wrap_err_with(|| format!("error reading key file {}", key_path.display()))?
        .pop()
        .ok_or_else(|| eyre!("no private key found in {}", key_path.display()))?;

    Ok((certs, PrivateKey(key)))
}

/**
Generate a TLS acceptor.

The `cert_path` should be a PEM file with a single full chain of certificates;
`key_path` should contain the single corresponding private key.
*/
pub fn load_acceptor<C, K>(cert_path: C, key_path: K) -> eyre::Result<TlsAcceptor>
where
    C: AsRef<Path>,
    K: AsRef<Path>,
{
    let (certs, key) = load_certs_and_key(cert_path.as_ref(), key_path.as_ref())?;

    let cfg = ServerConfig::builder()
        .with_safe_defaults()
        .with_no_client_auth()
        .with_single_cert(certs, key)
        .wrap_err("error building TLS configuration")?;

    Ok(TlsAcceptor::from(cfg))
}

#[cfg(test)]
mod test {
    use super::*;

    static CERT_FILE: &str = "certs/fullchain2.pem";
    static KEY_FILE: &str = "certs/privkey2.pem";

    #[test]
    fn make_tls_acceptor() -> eyre::Result<()> {
        let _acceptor = load_acceptor(CERT_FILE, KEY_FILE)?;
        Ok(())
    }
}
