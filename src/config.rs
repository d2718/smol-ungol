/*!
Reading configuration from a file.
*/
use std::{
    collections::BTreeMap,
    fmt::{Debug, Formatter},
    os::unix::fs::PermissionsExt,
    path::{Path, PathBuf},
};

use async_tls::TlsAcceptor;
use eyre::{bail, eyre, WrapErr};
use http::HeaderValue;
use serde::Deserialize;
use tracing::{event, Level};

use crate::{bl::Blacklister, layer::comp::Algo, mime::MimeMap, rsp::DEFAULT_CHUNK_SIZE};

const WORLD_READABLE: u32 = 0o004;

const DEFAULT_HTTP_PORT: u16 = 80;
const DEFAULT_HTTPS_PORT: u16 = 443;

const DEFAULT_MIN_COMPRESS_SIZE: u64 = 1024;
const DEFAULT_COMP_TYPES: &[Algo] = &[Algo::Br, Algo::Gzip, Algo::Deflate];

static DEFAULT_LOG_FILE: &str = "su.log";

pub struct HttpsConfig {
    pub port: u16,
    pub acceptor: TlsAcceptor,
}

impl Debug for HttpsConfig {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("HttpsConfig")
            .field("port", &self.port)
            .field("acceptor", &"[opaque]")
            .finish()
    }
}

#[derive(Clone, Copy, Default, Debug)]
pub struct RateConfig {
    pub n_requests: Option<u32>,
    pub time_interval: Option<u32>,
    pub prune_interval: Option<u32>,
}

/**
Specify compression behavior.

Default behavior is to use all three algorithms and compress anything larger
than 1 KiB. To use no compression, set `types` to an empty vector.
*/
#[derive(Clone, Debug, Deserialize)]
pub struct CompressionConfig {
    /// Algorithms to use. Possible values: `["gzip", "deflate", "br"]`
    pub types: Option<Vec<String>>,
    /// Minimum file size (in bytes) that will be compressed; files smaller
    /// than this will be transmitted uncompressed.
    pub min_size: Option<u64>,
    /// Do not compress these types regardless of Accept-Encoding.
    pub ignore: Option<Vec<String>>,
    /// Do not include the default ignored types in the ignore list
    /// by default. (Not recommended--the default ignored types are
    /// types that are all already compressed.)
    pub no_default_ignores: Option<bool>,
}

#[derive(Clone, Debug)]
pub struct CmpCfg {
    pub types: Vec<Algo>,
    pub min_size: u64,
    pub ignore: Vec<HeaderValue>,
    pub no_default_ignores: bool,
}

impl CmpCfg {
    fn try_from(f: CompressionConfig) -> eyre::Result<Option<CmpCfg>> {
        let types: Vec<Algo> = match f.types {
            None => DEFAULT_COMP_TYPES.to_vec(),
            Some(types) => {
                if types.is_empty() {
                    return Ok(None);
                }

                let types: Vec<Algo> = types
                    .iter()
                    .map(|t| t.parse())
                    .collect::<Result<Vec<_>, _>>()
                    .map_err(|e| {
                        eyre!(
                            "one or more invalid compression types: {:?} ({})",
                            &types,
                            &e
                        )
                    })?;
                types
            }
        };

        let ignore = match f.ignore {
            None => Vec::new(),
            Some(v) => v
                .into_iter()
                .map(|s| HeaderValue::from_str(s.trim().to_ascii_lowercase().as_str()))
                .collect::<Result<Vec<_>, _>>()?,
        };

        Ok(Some(CmpCfg {
            types,
            min_size: f.min_size.unwrap_or(DEFAULT_MIN_COMPRESS_SIZE),
            ignore,
            no_default_ignores: f.no_default_ignores.unwrap_or(false),
        }))
    }
}

impl Default for CmpCfg {
    fn default() -> Self {
        CmpCfg {
            types: DEFAULT_COMP_TYPES.to_vec(),
            min_size: DEFAULT_MIN_COMPRESS_SIZE,
            ignore: Vec::new(),
            no_default_ignores: false,
        }
    }
}

/**
Syntax of configuration file.

This struct doesn't need to be public, but this is an easy way to get
`cargo doc` to document the options in the configuration file.
*/

#[derive(Debug, Deserialize)]
pub struct CfgFile {
    /// The root directory from which to serve files.
    pub root: PathBuf,
    /// Port on which to listen for HTTP requests (if `ssl_cert_file` and
    /// `ssl_key_file` are supplied, the default is to _not_ respond to
    /// insecure requests; otherwise the default is 80).
    pub http_port: Option<u16>,
    /// Port on which to listen for HTTPS requests (if `ssl_cert_file` and
    /// `ssl_key_file` are supplied, the default is 443, otherwise the
    /// default is to not listen for secure requests).
    pub https_port: Option<u16>,
    /// File that contains the full trust chain of your SSL certificate.
    pub ssl_cert_file: Option<PathBuf>,
    /// File that contains the SSL certificate private key.
    pub ssl_key_file: Option<PathBuf>,
    /// If run as root, instead of chrooting to the `root` directory,
    /// drop privileges and run as this user.
    pub run_as: Option<String>,
    /// If this option is defined, requests that don't have a `Host`
    /// header value equal to one of these will be dropped.
    pub hosts: Option<Vec<String>>,
    /// IP addresses from which to block requests.
    pub blacklist: Option<Vec<String>>,
    /// Number of requests to allow in any given time window.
    pub rate_requests: Option<u32>,
    /// The length of the time window (in seconds).
    pub rate_seconds: Option<u32>,
    /// The number of requests after which the rate-limiter pauses to prune
    /// its tree of limit counters.
    pub prune_period: Option<u32>,
    /// Specify compression behavior.
    pub compression: Option<CompressionConfig>,
    /// Maximum size (in bytes) of chunks sent when chunking response bodies.
    /// This reduces the size of the chunking buffer (and thus the memory
    /// overhead) at the cost of possibly more read/write cycles per response.
    pub chunk_size: Option<usize>,
    /// File where requests will be logged.
    pub log_file: Option<PathBuf>,
    /// Supplemental mapping of file extensions to MIME types.
    pub mime_types: Option<BTreeMap<String, String>>,
}

impl CfgFile {
    fn https_config(&self) -> eyre::Result<Option<HttpsConfig>> {
        let (cert_file, key_file) = match (&self.ssl_cert_file, &self.ssl_key_file) {
            (Some(c), Some(k)) => (c, k),
            (Some(_), None) | (None, Some(_)) =>
                bail!("when specifying an `ssl_cert_file`, you must also specify an `ssl_key_file`, and vice versa"),
            (None, None) => return Ok(None),
        };

        let acceptor =
            crate::tls::load_acceptor(cert_file, key_file).wrap_err("error configuring TLS")?;
        let port = self.https_port.unwrap_or(DEFAULT_HTTPS_PORT);

        Ok(Some(HttpsConfig { port, acceptor }))
    }

    fn rate_config(&self) -> Option<RateConfig> {
        if matches!(
            (self.rate_requests, self.rate_seconds, self.prune_period),
            (None, None, None)
        ) {
            return None;
        }

        let rate_cfg = RateConfig {
            n_requests: self.rate_requests,
            time_interval: self.rate_seconds,
            prune_interval: self.prune_period,
        };
        Some(rate_cfg)
    }
}

/**
The actual configuration derived from a deserialized `CfgFile`.
*/
#[derive(Debug)]
pub struct Cfg {
    pub root: PathBuf,
    pub http_port: Option<u16>,
    pub https_config: Option<HttpsConfig>,
    pub hosts: Option<Vec<HeaderValue>>,
    pub run_as: Option<String>,
    pub blacklist: Option<Blacklister>,
    pub rate_config: Option<RateConfig>,
    pub comp_config: Option<CmpCfg>,
    pub chunk_size: usize,
    pub log_file: PathBuf,
    pub mime_types: MimeMap,
}

impl Cfg {
    pub fn from_file<P: AsRef<Path>>(p: P) -> eyre::Result<Cfg> {
        let p = p.as_ref();
        let bytes = std::fs::read_to_string(p)?;
        let f: CfgFile = toml::from_str(&bytes)?;
        event!(Level::DEBUG, "config file deserialized:\n{:#?}", &f);

        let root = f.root.canonicalize().wrap_err("bad root path specified")?;
        {
            let md = std::fs::metadata(&root).wrap_err("unable to get metadata for root path")?;
            if md.permissions().mode() & WORLD_READABLE == 0 {
                bail!("root path not world-readable");
            }
        }

        let https_config = f.https_config()?;

        let http_port = match f.http_port {
            None => {
                if https_config.is_some() {
                    None
                } else {
                    Some(DEFAULT_HTTP_PORT)
                }
            }
            Some(port) => Some(port),
        };

        let rate_config = f.rate_config();

        let hosts = match f.hosts {
            None => None,
            Some(v) => {
                let hosts = v
                    .iter()
                    .map(|val| {
                        HeaderValue::from_str(val.trim())
                            .wrap_err_with(|| eyre!("invalid \"hosts\" value: {}", val))
                    })
                    .collect::<eyre::Result<Vec<_>>>()?;
                Some(hosts)
            }
        };

        let run_as = f.run_as;

        let log_file = f
            .log_file
            .unwrap_or_else(|| PathBuf::from(DEFAULT_LOG_FILE));

        let blacklist = match f.blacklist {
            Some(vec) => Some(
                Blacklister::try_from(vec.as_slice())
                    .wrap_err("error parsing blacklist IP addresses")?,
            ),
            None => None,
        };

        let comp_config = match f.compression {
            None => Some(CmpCfg::default()),
            Some(ccfg) => CmpCfg::try_from(ccfg)?,
        };

        let chunk_size = f.chunk_size.unwrap_or(DEFAULT_CHUNK_SIZE);

        let mime_types = {
            let mut mime_types = MimeMap::default();
            if let Some(map) = f.mime_types {
                for (k, v) in map.into_iter() {
                    mime_types.try_insert(&k, v)?;
                }
            }
            mime_types
        };

        let cfg = Cfg {
            root,
            http_port,
            https_config,
            hosts,
            run_as,
            blacklist,
            rate_config,
            comp_config,
            chunk_size,
            log_file,
            mime_types,
        };
        Ok(cfg)
    }

    pub fn will_chroot(&self) -> bool {
        self.run_as.is_none() && crate::root::is_running_as_root()
    }
}
