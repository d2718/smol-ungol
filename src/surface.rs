/*!
The outermost layer.
*/
use eyre::WrapErr;

use http::{header, HeaderValue};
use smol::{io::AsyncWriteExt, net::SocketAddr};
use tracing::{event, Level};

use crate::{
    bl::Blacklister,
    layer::Layer,
    req::{EitherStream, SuRequest},
};

pub struct Surface<L> {
    /// The outermost wrapped layer.
    inner: L,
    /// The blacklist layer.
    blacklist: Option<Blacklister>,
    hosts: Option<Vec<HeaderValue>>,
    chunk_size: usize,
}

impl<L: Layer> Surface<L> {
    /// Create a new `Surface`, wrapping the next `Layer`.
    pub fn new(inner: L, chunk_size: usize) -> Surface<L> {
        Surface {
            inner,
            blacklist: None,
            hosts: None,
            chunk_size,
        }
    }

    /// Builder-pattern method to add a `Blacklister`.
    pub fn with_blacklist(self, blacklist: Option<Blacklister>) -> Surface<L> {
        Surface { blacklist, ..self }
    }

    pub fn with_hosts(self, hosts: Option<Vec<HeaderValue>>) -> Surface<L> {
        Surface { hosts, ..self }
    }

    pub async fn handle(&self, mut stream: EitherStream, addr: SocketAddr) -> eyre::Result<()> {
        if let Some(blist) = &self.blacklist {
            if blist.is_blacklisted(&addr) {
                stream
                    .close()
                    .await
                    .wrap_err("error closing blacklisted socket")?;
                return Ok(());
            }
        }

        let (incoming, mut outgoing) = smol::io::split(stream);

        let request = match SuRequest::read_request(incoming, addr).await {
            Ok(request) => request,
            Err(e) => {
                if let Err(e) = crate::rsp::blow(outgoing, e.to_response(), self.chunk_size).await {
                    event!(
                        Level::ERROR,
                        "error writing error response to client: {}",
                        &e
                    );
                }
                return Ok(());
            }
        };

        {
            event!(Level::DEBUG, "about to match Host headers to self.host");
            match &self.hosts {
                None => event!(Level::DEBUG, "    self.hosts is None"),
                Some(ref hosts) => {
                    event!(Level::DEBUG, "    self.hosts:");
                    for h in hosts.iter() {
                        event!(Level::DEBUG, "        {:?}", h.to_str());
                    }
                }
            }

            match request.inner().headers().get(header::HOST) {
                None => event!(Level::DEBUG, "    Host: header is None"),
                Some(val) => event!(Level::DEBUG, "    Host: {:?}", val.to_str()),
            }
        }

        match (&self.hosts, request.inner().headers().get(header::HOST)) {
            (Some(hosts), Some(host)) => {
                if !hosts.contains(host) {
                    outgoing
                        .close()
                        .await
                        .wrap_err("error closing stream with invalid Host header")?;
                    return Ok(());
                }
            }
            (Some(_), None) => {
                outgoing
                    .close()
                    .await
                    .wrap_err("error closing stream with invalid Host header")?;
                return Ok(());
            }
            _ => {}
        }

        if let Some(response) = self.inner.call(request).await {
            if let Err(e) = crate::rsp::blow(outgoing, response, self.chunk_size).await {
                event!(Level::ERROR, "error writing respones to client: {}", &e);
            }
        }

        Ok(())
    }
}
