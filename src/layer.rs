/*!
The `Layer` trait for simplifying the various "stages" of the HTTP
request-reqponse cycle.
*/
pub mod comp;
//mod cors;
mod misc;
pub use comp::Compression;
pub use misc::*;

use crate::{req::SuRequest, SuResponse};

/**
The `Layer` represents a layer of processing/filtering that a
request/response pair passes through. It is "symmetrical" in that it
has the opportunity to act on any given request on the way in, as well
as its corresponding response on the way out. Because both of these
happen in the course of a single method call, it's trivial to preserve
some state between request and response if desired.

The intent is for each successive layer to wrap the one "beneath" it,
passing incoming requests "down" the stack of layers by calling its
wrapped layer's `.call()` method. It will then have returning responses
"bubble up" through the layer stack in the form of that `.call()` method's
return value.
*/
pub trait Layer {
    #[allow(async_fn_in_trait)]
    async fn call(&self, input: SuRequest) -> Option<SuResponse>;
}

/// A no-op layer.
pub struct PassLayer<L> {
    inner: L,
}

impl<L> PassLayer<L> {
    pub fn new(inner: L) -> Self {
        Self { inner }
    }
}

impl<L: Layer> Layer for PassLayer<L> {
    async fn call(&self, input: SuRequest) -> Option<SuResponse> {
        self.inner.call(input).await
    }
}

/// A layer that can be configured to be one of two different layers.
pub enum EitherLayer<L, R> {
    Left(L),
    Right(R),
}

impl<L, R> EitherLayer<L, R> {
    pub fn left(inner: L) -> Self {
        Self::Left(inner)
    }

    pub fn right(inner: R) -> Self {
        Self::Right(inner)
    }
}

impl<L, R> Layer for EitherLayer<L, R>
where
    L: Layer,
    R: Layer,
{
    async fn call(&self, request: SuRequest) -> Option<SuResponse> {
        match self {
            Self::Left(ref inner) => inner.call(request).await,
            Self::Right(ref inner) => inner.call(request).await,
        }
    }
}
