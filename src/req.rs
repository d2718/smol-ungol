/*!
Reading requests.
*/
use std::{
    pin::Pin,
    task::{Context, Poll},
};

use async_tls::server::TlsStream;
use http::{
    header::{HeaderName, HeaderValue},
    request::Builder,
    Method, Request, StatusCode, Version,
};
use pin_project::pin_project;
use smol::{
    io::{AsyncBufReadExt, AsyncRead, AsyncWrite, BufReader, ReadHalf},
    net::{SocketAddr, TcpStream},
};
use tracing::{event, span, Level};

use crate::errors::SuErr;

const BUFFER_SIZE: usize = 1024;

// Split a string around the first occurrence of `token`, and return a tuple
// of `(before, after)` chunks.
//
// This is for parsing `name: value` HTTP header lines.
fn split_at<'a>(src: &'a str, token: &str) -> Option<(&'a str, &'a str)> {
    let idx = src.find(token)?;
    let after_idx = idx + token.len();
    Some((&src[..idx], &src[after_idx..]))
}

/// Unifying type for TLS-encryped and unencrypted request streams.
#[pin_project(project = EitherProj)]
#[derive(Debug)]
pub enum EitherStream {
    Plain(#[pin] TcpStream),
    Tls(#[pin] TlsStream<TcpStream>),
}

impl AsyncRead for EitherStream {
    fn poll_read(
        self: Pin<&mut Self>,
        cx: &mut Context,
        buff: &mut [u8],
    ) -> Poll<Result<usize, smol::io::Error>> {
        match self.project() {
            EitherProj::Plain(stream) => stream.poll_read(cx, buff),
            EitherProj::Tls(stream) => stream.poll_read(cx, buff),
        }
    }
}

impl AsyncWrite for EitherStream {
    fn poll_write(
        self: Pin<&mut Self>,
        cx: &mut Context,
        buff: &[u8],
    ) -> Poll<Result<usize, smol::io::Error>> {
        match self.project() {
            EitherProj::Plain(stream) => stream.poll_write(cx, buff),
            EitherProj::Tls(stream) => stream.poll_write(cx, buff),
        }
    }

    fn poll_flush(self: Pin<&mut Self>, cx: &mut Context) -> Poll<Result<(), smol::io::Error>> {
        match self.project() {
            EitherProj::Plain(stream) => stream.poll_flush(cx),
            EitherProj::Tls(stream) => stream.poll_flush(cx),
        }
    }

    fn poll_close(self: Pin<&mut Self>, cx: &mut Context) -> Poll<Result<(), smol::io::Error>> {
        match self.project() {
            EitherProj::Plain(stream) => stream.poll_close(cx),
            EitherProj::Tls(stream) => stream.poll_close(cx),
        }
    }
}

impl From<TcpStream> for EitherStream {
    fn from(s: TcpStream) -> Self {
        EitherStream::Plain(s)
    }
}

impl From<TlsStream<TcpStream>> for EitherStream {
    fn from(s: TlsStream<TcpStream>) -> Self {
        EitherStream::Tls(s)
    }
}

/// Represents an incoming request, together with some "metadata" not
/// carried by `[Request]`. (Currently only the client address.)
///
/// If there's any body to the request, the wrapped client hasn't read
/// it yet.
#[derive(Debug)]
pub struct SuRequest {
    pub addr: SocketAddr,
    pub request: Request<BufReader<ReadHalf<EitherStream>>>,
}

impl SuRequest {
    /// Read the header section of a request, and return a request `Builder`
    /// populated with that information.
    async fn suck_headers<R>(r: &mut BufReader<ReadHalf<R>>) -> Result<Builder, SuErr>
    where
        R: AsyncRead + Unpin,
    {
        let mut buff = String::with_capacity(BUFFER_SIZE);
        r.read_line(&mut buff).await?;
        let mut chunks = buff.trim().split_ascii_whitespace();

        let verb = chunks.next().ok_or_else(|| {
            SuErr::blank(
                StatusCode::BAD_REQUEST,
                format!("malformed request line: {}", &buff),
            )
        })?;
        let verb = match verb {
            "GET" => Method::GET,
            "POST" => Method::POST,
            "HEAD" => Method::HEAD,
            "OPTIONS" => Method::OPTIONS,
            "TRACE" => Method::TRACE,
            "PUT" => Method::PUT,
            "DELETE" => Method::DELETE,
            "CONNECT" => Method::CONNECT,
            "PATCH" => Method::PATCH,
            word => {
                return Err(SuErr::blank(
                    StatusCode::BAD_REQUEST,
                    format!("invalid VERB: {}", word),
                ))
            }
        };

        let rsrc = chunks
            .next()
            .ok_or_else(|| SuErr::blank(StatusCode::BAD_REQUEST, "no RESOURCE string"))?;

        let vers = chunks
            .next()
            .ok_or_else(|| SuErr::blank(StatusCode::BAD_REQUEST, "no VERSION string"))?;
        let vers = match vers {
            "HTTP/0.9" => Version::HTTP_09,
            "HTTP/1.0" => Version::HTTP_10,
            "HTTP/1.1" => Version::HTTP_11,
            "HTTP/2.0" => Version::HTTP_2,
            "HTTP/3.0" => Version::HTTP_3,
            chunk => {
                return Err(SuErr::blank(
                    StatusCode::BAD_REQUEST,
                    format!("invalid VERSION: {}", chunk),
                ))
            }
        };

        event!(Level::DEBUG, "first line: {:?} {} {:?}", &verb, rsrc, &vers);

        let mut req = Request::builder().method(&verb).uri(rsrc).version(vers);

        buff.clear();
        if r.read_line(&mut buff).await? == 0 {
            return Ok(req);
        }

        while let Some((name, val)) = split_at(&buff, ": ") {
            let (name, val) = (name.trim(), val.trim());
            let hname = HeaderName::from_bytes(name.as_bytes()).map_err(|e| {
                SuErr::message(
                    StatusCode::BAD_REQUEST,
                    format!("request had invalid header name ({}): {}", &e, &buff),
                    format!("invalid header: {}", &buff),
                )
            })?;
            let hval = HeaderValue::from_str(val).map_err(|e| {
                SuErr::message(
                    StatusCode::BAD_REQUEST,
                    format!("request had invalid header value ({}): {}", &e, &buff),
                    format!("invalid header: {}", &buff),
                )
            })?;

            event!(Level::DEBUG, "    header: {}: {}", name, val);

            req = req.header(hname, hval);
            buff.clear();
            if r.read_line(&mut buff).await? == 0 {
                return Ok(req);
            }
        }

        Ok(req)
    }

    /// Given the read half of a fresh TCP connection, parse the incoming
    /// request. Does not parse or consume any body that may be present;
    /// use the `.reader()` method to get at the underlying reader.
    pub async fn read_request(r: ReadHalf<EitherStream>, addr: SocketAddr) -> Result<Self, SuErr> {
        span!(Level::TRACE, "SuRequest::read_request( [TcpStream] )");
        let mut r = BufReader::new(r);
        let req = SuRequest::suck_headers(&mut r).await?;
        let request = req.body(r)?;

        Ok(Self { addr, request })
    }

    /// Get an immutable handle to the underlying `[Request]`. To suck
    /// the body out, use `[SuRequest::reader]`.
    pub fn inner(&self) -> &Request<BufReader<ReadHalf<EitherStream>>> {
        &self.request
    }

    /// Get a mutable (readable) handle from which the body of the
    /// request can be read.
    pub fn reader(&mut self) -> &mut BufReader<ReadHalf<EitherStream>> {
        self.request.body_mut()
    }
}
