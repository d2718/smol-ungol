/*!
A by-IP blacklister.

The implementation here is simple: maintain a collection of blacklisted IPs;
if an incoming IP address is on the blacklist, the handler should just
drop the connection.
*/
use std::{collections::BTreeSet, convert::TryFrom};

use eyre::WrapErr;
use smol::net::{IpAddr, SocketAddr};

#[derive(Debug, Default)]
pub struct Blacklister {
    list: BTreeSet<IpAddr>,
}

impl Blacklister {
    pub fn is_blacklisted(&self, socket_addr: &SocketAddr) -> bool {
        self.list.contains(&socket_addr.ip())
    }
}

impl<P> TryFrom<&[P]> for Blacklister
where
    P: AsRef<str>,
{
    type Error = eyre::Report;

    fn try_from(a: &[P]) -> eyre::Result<Self> {
        let mut list: BTreeSet<IpAddr> = BTreeSet::default();

        for s in a.iter() {
            let addr: IpAddr = s
                .as_ref()
                .parse()
                .wrap_err_with(|| format!("can't parse IP address from {}", s.as_ref()))?;
            list.insert(addr);
        }

        Ok(Self { list })
    }
}
