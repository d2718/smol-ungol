/*!
Per-IP request rate limiting.
*/
use std::{
    collections::BTreeMap,
    sync::atomic::{AtomicU32, Ordering},
};

use http::{Response, StatusCode};
use smol::{lock::Mutex, net::IpAddr};
use time::{Duration, Instant};

use crate::{body::Body, errors::SuErr, layer::Layer, SuRequest, SuResponse};

const DEFAULT_LIMIT: u32 = 24;
const DEFAULT_COOLDOWN: Duration = Duration::seconds(3);
const DEFAULT_PRUNE_INTERVAL: u32 = 1024;

// How any given request should be responded to.
#[derive(Clone, Copy, Debug)]
enum Action {
    // Go ahead, serve this request.
    Serve,
    // Respond with TOO_MANY_REQUESTS
    Shed,
}

#[derive(Clone, Copy, Debug)]
struct Counter {
    oldest: Instant,
    count: u32,
}

impl Counter {
    fn new() -> Counter {
        Counter {
            oldest: Instant::now(),
            count: 1,
        }
    }

    fn is_expired(&self, cooldown: Duration) -> bool {
        self.oldest.elapsed() > cooldown
    }

    fn hit(&mut self, limit: u32, cooldown: Duration) -> Action {
        if self.is_expired(cooldown) {
            self.count = 1;
            self.oldest = Instant::now();
            Action::Serve
        } else if self.count < limit {
            self.count += 1;
            Action::Serve
        } else {
            Action::Shed
        }
    }
}

impl Default for Counter {
    fn default() -> Self {
        Counter::new()
    }
}

pub struct Limiter<L> {
    inner: L,
    counts: Mutex<BTreeMap<IpAddr, Counter>>,
    limit: u32,
    cooldown: Duration,
    prune_interval: u32,
    prune_counter: AtomicU32,
}

impl<L> Limiter<L> {
    pub fn new(inner: L, limit: u32, cooldown: Duration, prune_interval: u32) -> Limiter<L> {
        Limiter {
            inner,
            limit,
            cooldown,
            prune_interval,
            prune_counter: AtomicU32::new(0),
            counts: Mutex::new(BTreeMap::default()),
        }
    }

    pub fn with_defaults(inner: L) -> Limiter<L> {
        Limiter::new(
            inner,
            DEFAULT_LIMIT,
            DEFAULT_COOLDOWN,
            DEFAULT_PRUNE_INTERVAL,
        )
    }

    pub fn with_limit(mut self, n: u32) -> Limiter<L> {
        self.limit = n;
        self
    }

    pub fn with_cooldown(mut self, d: Duration) -> Limiter<L> {
        self.cooldown = d;
        self
    }

    pub fn with_interval(mut self, n: u32) -> Limiter<L> {
        self.prune_interval = n;
        self
    }

    pub fn stats(&self) -> (u32, Duration, u32) {
        (self.limit, self.cooldown, self.prune_interval)
    }

    async fn prune(&self) {
        self.counts
            .lock()
            .await
            .retain(|_, counter| !counter.is_expired(self.cooldown));
    }

    async fn hit_from(&self, addr: IpAddr) -> Action {
        if self.prune_counter.fetch_add(1, Ordering::Relaxed) > self.prune_interval {
            self.prune().await;
            self.prune_counter.store(0, Ordering::Relaxed);
        }

        self.counts
            .lock()
            .await
            .entry(addr)
            .or_default()
            .hit(self.limit, self.cooldown)
    }
}

impl<L: Layer> Layer for Limiter<L> {
    async fn call(&self, input: SuRequest) -> Option<SuResponse> {
        let action = self.hit_from(input.addr.ip()).await;
        match action {
            Action::Serve => self.inner.call(input).await,
            Action::Shed => {
                match Response::builder()
                    .status(StatusCode::TOO_MANY_REQUESTS)
                    .body(Body::Empty)
                {
                    Ok(response) => Some(response),
                    Err(e) => Some(SuErr::from(e).to_response()),
                }
            }
        }
    }
}
