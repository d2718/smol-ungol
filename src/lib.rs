pub mod base;
pub mod bl;
pub mod body;
pub mod config;
pub mod errors;
pub mod layer;
pub mod mime;
pub mod rate;
pub mod req;
pub mod root;
pub mod rsp;
pub mod surface;
pub mod tls;

use body::Body;
use http::{header::AsHeaderName, Response};
use once_cell::sync::Lazy;

pub static VERSION: &str = env!("CARGO_PKG_VERSION");
pub static SERVER: Lazy<String> = Lazy::new(|| format!("smol-ungol/{}", VERSION));

pub use req::SuRequest;
pub type SuResponse = Response<Body>;

pub fn request_header<H: AsHeaderName>(r: &SuRequest, h: H) -> Option<&str> {
    if let Some(val) = r.inner().headers().get(h) {
        if let Ok(s) = val.to_str() {
            return Some(s);
        }
    }

    None
}

pub fn response_header<H: AsHeaderName>(r: &SuResponse, h: H) -> Option<&str> {
    if let Some(val) = r.headers().get(h) {
        if let Ok(s) = val.to_str() {
            return Some(s);
        }
    }
    None
}

#[cfg(test)]
mod test {
    use std::sync::RwLock;
    use tracing::{event, Level};

    static LOGGING_STARTED: RwLock<bool> = RwLock::new(false);

    /// Start logging during testing if logging hasn't already been started.
    pub fn start_logging() {
        use tracing_subscriber::{fmt, prelude::*, EnvFilter};

        if *LOGGING_STARTED.read().unwrap() {
            return;
        }

        *LOGGING_STARTED.write().unwrap() = true;

        tracing_subscriber::registry()
            .with(fmt::layer())
            .with(EnvFilter::from_default_env())
            .init();
        event!(Level::INFO, "logging started");
    }
}
