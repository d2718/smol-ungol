/*!
Functions for minimizing root access.

In order to bind to the standard ports 80 and 443, a program must run
as root. Obviously, this presents a security risk, particularly for
software (like a web server) that is in contact with the Internet at Large.

This module offers a choice of two functions to help mitigate this risk:

  * `drop_to_user()` attempts to drop root privileges and run as a
    regular user.
  * `chroot_to()` attempts to chroot to a directory (this should be
    the root of the directory exposed to the web).

If smol-ungol is run as root, and it can't successfully call one of
these, it'll refuse to go any further.
*/
use std::{ffi::CString, path::Path};

use eyre::{bail, eyre, WrapErr};
use tracing::{event, Level};

pub fn is_running_as_root() -> bool {
    event!(Level::TRACE, "is_running_as_root() called");

    let uid = unsafe { libc::getuid() };
    uid == 0
}

/// Attempt to drop root priviliges to run as the given user.
pub fn drop_to_user(uname: &str) -> eyre::Result<()> {
    event!(Level::TRACE, "drop_to_user({:?}) called", uname);

    let c_uname = CString::new(uname)
        .wrap_err_with(|| format!("unable to create c-style string for user name {}", uname))?;
    let pw_record = unsafe { libc::getpwnam(c_uname.as_ptr()) };
    if pw_record.is_null() {
        bail!("unable to get uid and gid for user {}", uname);
    }

    let uid = unsafe { (*pw_record).pw_uid };
    let gid = unsafe { (*pw_record).pw_gid };
    if unsafe { libc::setgid(gid) } != 0 {
        bail!("unable to set gid {} ({})", gid, uname);
    }
    if unsafe { libc::setuid(uid) } != 0 {
        bail!("unable to set uid {} ({})", uid, uname);
    }

    event!(Level::INFO, "now running as user {}", uname);

    Ok(())
}

/// Attempt to chroot to the given directory.
pub fn chroot<P: AsRef<Path>>(p: P) -> eyre::Result<()> {
    let p = p.as_ref();
    event!(Level::TRACE, "chroot({}) called", p.display());

    std::os::unix::fs::chroot(p).wrap_err_with(|| eyre!("failed to chroot to {}", p.display()))?;
    std::env::set_current_dir("/").wrap_err("failed to set working dir to webroot")?;

    Ok(())
}
