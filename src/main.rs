use async_tls::TlsAcceptor;
use eyre::WrapErr;
use http::header;
use smol::{
    net::{SocketAddr, TcpListener},
    LocalExecutor,
};
use tracing::{event, Level};
use tracing_subscriber::{fmt, prelude::*, EnvFilter};

use smol_ungol::{
    base::Base, config::Cfg, layer::*, rate::Limiter, root, surface::Surface, SERVER,
};

async fn listen_plain<L: Layer>(
    exec: &LocalExecutor<'static>,
    listener: TcpListener,
    stack: &'static Surface<L>,
) {
    loop {
        let (stream, addr) = match listener.accept().await {
            Ok(pair) => pair,
            Err(e) => {
                event!(Level::ERROR, "error accepting request: {}", &e);
                continue;
            }
        };

        exec.spawn(async move {
            if let Err(e) = stack.handle(stream.into(), addr).await {
                event!(Level::ERROR, "error handling request: {}", &e);
            }
        })
        .detach()
    }
}

async fn listen_tls<L: Layer>(
    exec: &LocalExecutor<'static>,
    listener: TcpListener,
    acceptor: &'static TlsAcceptor,
    stack: &'static Surface<L>,
) {
    loop {
        let (stream, addr) = match listener.accept().await {
            Ok(pair) => pair,
            Err(e) => {
                event!(Level::ERROR, "error accepting request: {}", &e);
                continue;
            }
        };

        exec.spawn(async move {
            match acceptor.accept(stream).await {
                Ok(tls_stream) => {
                    if let Err(e) = stack.handle(tls_stream.into(), addr).await {
                        event!(Level::ERROR, "error handling request: {}", &e);
                    }
                }
                Err(e) => event!(Level::ERROR, "error negotiating TLS handshake: {}", &e),
            }
        })
        .detach();
    }
}

fn main() -> eyre::Result<()> {
    tracing_subscriber::registry()
        .with(fmt::layer())
        .with(EnvFilter::from_default_env())
        .init();

    let cfg_file = std::env::args()
        .nth(1)
        .unwrap_or("./config.toml".to_owned());

    let cfg = Cfg::from_file(&cfg_file)
        .wrap_err_with(|| format!("unable to read config file {}", &cfg_file))?;

    let base = if cfg.will_chroot() {
        Base::new("/", cfg.mime_types)
    } else {
        Base::new(&cfg.root, cfg.mime_types)
    };

    let comp_layer = match cfg.comp_config {
        None => EitherLayer::left(base),
        Some(cfg) => {
            event!(Level::INFO, "compression configuration: {:?}", &cfg);
            EitherLayer::right(Compression::new(base, cfg))
        }
    };

    let decap_layer = Decapitator::new(comp_layer);

    let add_headers = AddHeaders::new(decap_layer).with(
        header::SERVER,
        http::HeaderValue::from_str(&SERVER).unwrap(),
    );

    let exec = Box::leak(Box::new(LocalExecutor::new()));

    smol::block_on(exec.run(async {
        let log_layer = Logging::new(
            add_headers,
            smol::fs::OpenOptions::new()
                .append(true)
                .open(&cfg.log_file)
                .await
                .unwrap(),
        );

        let limit_layer = match cfg.rate_config {
            None => EitherLayer::left(log_layer),
            Some(cfg) => {
                let mut lim = Limiter::with_defaults(log_layer);
                if let Some(n) = cfg.n_requests {
                    lim = lim.with_limit(n);
                }
                if let Some(n) = cfg.time_interval {
                    lim = lim.with_cooldown(time::Duration::seconds(n.into()));
                }
                if let Some(n) = cfg.prune_interval {
                    lim = lim.with_interval(n);
                }
                event!(Level::INFO, "rate limit (r, t, prune): {:?}", lim.stats());
                EitherLayer::right(lim)
            }
        };

        let surface = Surface::new(limit_layer, cfg.chunk_size)
            .with_blacklist(cfg.blacklist)
            .with_hosts(cfg.hosts);

        let stack = Box::leak(Box::new(surface));

        let https_task = if let Some(https) = cfg.https_config {
            let listener = TcpListener::bind(SocketAddr::new([0, 0, 0, 0].into(), https.port))
                .await
                .wrap_err_with(|| format!("unable to bind HTTPS listener to port {}", &https.port))
                .unwrap();
            let acceptor = Box::leak(Box::new(https.acceptor));
            event!(
                Level::INFO,
                "accepting HTTPS connections on {}",
                listener.local_addr().unwrap()
            );

            let execref = &*exec;
            let stackref = &*stack;
            Some(exec.spawn(async {
                listen_tls(execref, listener, acceptor, stackref).await;
            }))
        } else {
            None
        };

        let http_task = if let Some(port) = cfg.http_port {
            let listener = TcpListener::bind(SocketAddr::new([0, 0, 0, 0].into(), port))
                .await
                .wrap_err_with(|| format!("unable to bind HTTP listener to port {}", &port))
                .unwrap();
            event!(
                Level::INFO,
                "accepting HTTP connections on {}",
                listener.local_addr().unwrap()
            );

            let execref = &*exec;
            let stackref = &*stack;
            Some(exec.spawn(async {
                listen_plain(execref, listener, stackref).await;
            }))
        } else {
            None
        };

        if root::is_running_as_root() {
            if let Some(uname) = cfg.run_as {
                root::drop_to_user(&uname).unwrap();
                event!(Level::INFO, "running as user {}", &uname);
            } else {
                root::chroot(&cfg.root).unwrap();
                event!(
                    Level::INFO,
                    "running jailed in directory {}",
                    std::env::current_dir().unwrap().display()
                );
            }
        }

        match (http_task, https_task) {
            (Some(t), Some(u)) => {
                smol::future::zip(t, u).await;
            }
            (Some(t), None) => {
                t.await;
            }
            (None, Some(u)) => {
                u.await;
            }
            (None, None) => {
                eprintln!("Shutting down; nothing to do.");
            }
        }
    }));

    Ok(())
}
