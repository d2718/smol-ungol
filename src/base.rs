/*!
Actually serving files.
*/
mod dir;

use std::{
    convert::TryFrom,
    io::Write,
    path::{Component, Path, PathBuf},
};

use eyre::{bail, WrapErr};
use http::{
    header::{self, HeaderValue},
    response::Builder,
    HeaderMap, Method, Response, StatusCode,
};
use once_cell::sync::Lazy;
use regex::Regex;
use smol::{
    fs::{metadata, unix::PermissionsExt, File, Metadata},
    io::{AsyncReadExt, AsyncSeekExt, SeekFrom},
};
use time::{format_description::well_known::Rfc2822, OffsetDateTime};
use tracing::{event, Level};

use crate::{
    body::Body,
    errors::SuErr,
    layer::Layer,
    mime::{MimeMap, OCTET_STREAM},
    rsp::{COLON, CRLF},
    SuRequest, SuResponse,
};

// Bitwise-anding a permissions value with this should yield a non-zero
// value if it's world-readable.
const READ_PERMISSION: u32 = 0o004;

static RANGE_RE: Lazy<Regex> = Lazy::new(|| {
    Regex::new(r#"^\s*bytes=(\d+)?-(\d+)?"#).expect("unable to initialize range header regex")
});

/// Generate a response to a TRACE request.
fn respond_to_trace(req: SuRequest) -> Result<SuResponse, SuErr> {
    event!(Level::TRACE, "respond_to_trace( ... )");

    let (parts, _) = req.request.into_parts();
    let mut buff: Vec<u8> = Vec::new();

    writeln!(
        &mut buff,
        "{} {} {:?}",
        &parts.method, &parts.uri, &parts.version
    )?;
    for (name, val) in parts.headers.iter() {
        // If we ever end up marking headers as sensitive, we'll need this.
        // For now, definitely no one should be sending sensitive headers.
        // if val.is_sensitive() {
        //     continue;
        // }
        buff.write_all(name.as_str().as_bytes())?;
        buff.write_all(COLON)?;
        buff.write_all(val.as_bytes())?;
        buff.write_all(CRLF)?;
    }

    let r = Response::builder()
        .status(StatusCode::OK)
        .header(
            header::CONTENT_TYPE,
            HeaderValue::from_static("message/http"),
        )
        .header(header::CONTENT_LENGTH, buff.len())
        .body(buff.into())?;

    Ok(r)
}

/**
A wrapper for an `OffsetDateTime` to add some convencience functionality
for adding headers and determining if a Not Modified is appropriate.
*/
#[derive(Clone, Copy, Debug)]
struct LastModified(OffsetDateTime);

impl TryFrom<&Metadata> for LastModified {
    type Error = eyre::Report;

    fn try_from(md: &Metadata) -> Result<Self, Self::Error> {
        let sys_t = md.modified().wrap_err("cannot read modification time")?;
        let odt = OffsetDateTime::from(sys_t);
        Ok(LastModified(odt))
    }
}

impl TryFrom<&HeaderValue> for LastModified {
    type Error = eyre::Report;

    fn try_from(hval: &HeaderValue) -> eyre::Result<Self> {
        let hval = hval.to_str().wrap_err("header value not valid UTF-8")?;
        let odt = OffsetDateTime::parse(hval, &Rfc2822)
            .wrap_err("header value not parseable datetime")?;
        Ok(LastModified(odt))
    }
}

impl TryFrom<&LastModified> for HeaderValue {
    type Error = eyre::Report;

    fn try_from(lm: &LastModified) -> eyre::Result<HeaderValue> {
        let mut buff: [u8; 64] = [0u8; 64];
        let length =
            lm.0.format_into(&mut buff.as_mut_slice(), &Rfc2822)
                .wrap_err("error formatting datetime")?;
        let hval = HeaderValue::from_bytes(&buff[..length])
            .wrap_err("formatted datetime makes illegal heaer value")?;
        Ok(hval)
    }
}

impl TryFrom<&LastModified> for SuResponse {
    type Error = eyre::Report;

    fn try_from(lm: &LastModified) -> eyre::Result<SuResponse> {
        let r = Response::builder()
            .status(StatusCode::NOT_MODIFIED)
            .header(header::LAST_MODIFIED, HeaderValue::try_from(lm)?)
            .body(Body::Empty)?;
        Ok(r)
    }
}

impl LastModified {
    /// If we haven't been modified since the `If-Modified-Since` header
    /// from this request's headers, return a response saying so.
    fn check_if_modified(&self, headers: &HeaderMap) -> Option<SuResponse> {
        let last_date = LastModified::try_from(headers.get(header::IF_MODIFIED_SINCE)?).ok()?;

        if self.0 <= last_date.0 {
            SuResponse::try_from(self).ok()
        } else {
            None
        }
    }

    /// Add this date as a `Last-Modified` header to the given response.
    fn add_header(&self, r: Builder) -> Builder {
        let hval = match HeaderValue::try_from(self) {
            Ok(hval) => hval,
            Err(e) => {
                event!(Level::WARN, "{}", e);
                return r;
            }
        };

        r.header(header::LAST_MODIFIED, hval)
    }
}

fn parse_int(s: Option<&str>) -> Result<Option<i64>, ()> {
    let s = match s {
        None => return Ok(None),
        Some(s) => s,
    };

    match s.parse::<i64>() {
        Ok(n) => Ok(Some(n)),
        Err(_) => Err(()),
    }
}

#[derive(Clone, Copy, Debug)]
struct Range {
    start: u64,
    end: u64,
    filesize: u64,
}

impl Range {
    fn try_from_headers(headers: &HeaderMap, md: &Metadata) -> Result<Option<Range>, SuErr> {
        let header_val = match headers.get(header::RANGE) {
            None => return Ok(None),
            Some(val) => match val.to_str() {
                Err(_) => return Ok(None),
                Ok(s) => s,
            },
        };

        let caps = match RANGE_RE.captures(header_val) {
            None => return Ok(None),
            Some(caps) => caps,
        };

        let start = match parse_int(caps.get(1).map(|m| m.as_str())) {
            Err(_) => return Ok(None),
            Ok(n) => n,
        };
        let end = match parse_int(caps.get(2).map(|m| m.as_str())) {
            Err(_) => return Ok(None),
            Ok(n) => n,
        };

        let fsize = md.len() as i64;

        let (start, end) = match (start, end) {
            (None, None) => return Ok(None),
            (Some(n), None) => (n, fsize),
            (None, Some(m)) => ((fsize - m), fsize),
            (Some(n), Some(m)) => (n, m + 1),
        };

        if start < 0 || end <= start || end > fsize {
            return Err(SuErr::blank(
                StatusCode::RANGE_NOT_SATISFIABLE,
                format!("range not satisfiable: {}-{}/{}", start, end, fsize),
            ));
        }

        // We just made sure that `start` and `end` are both non-negative,
        // and `fsize` was just reported by the OS as a file size. These
        // casts should be okay.
        let r = Range {
            start: start as u64,
            end: end as u64,
            filesize: fsize as u64,
        };

        Ok(Some(r))
    }

    fn to_header(&self) -> Result<HeaderValue, SuErr> {
        let end = self.end.checked_sub(1).ok_or_else(|| {
            SuErr::blank(
                StatusCode::INTERNAL_SERVER_ERROR,
                "subtraction somehow underflowed during generation of Content-Range header",
            )
        })?;

        HeaderValue::try_from(format!("bytes {}-{}/{}", &self.start, &end, &self.filesize)).map_err(
            |e| {
                SuErr::blank(
                    StatusCode::INTERNAL_SERVER_ERROR,
                    format!("unable to generate valid Content-Range header: {}", &e),
                )
            },
        )
    }
}

/// Describes directory indexing behavior; that is, how the web server
/// should respond when a directory is requested.
#[derive(Clone, Debug)]
pub enum Index {
    /// Attempt to serve a file of the given name (like "index.html")
    /// from the directory.
    File(PathBuf),
    /// Automatically generate an index of files in the directory.
    Generate,
    /// Attempt to serve a file of the given name; if not present,
    /// generate an index.
    Fallback(PathBuf),
    /// Do not serve directory indices.
    No,
}

/// The "base layer" of the stack. Associates resource URLs with files
/// on the local system and serves them.
#[derive(Debug)]
pub struct Base {
    root: PathBuf,
    index: Index,
    mime: MimeMap,
}

impl Base {
    /// Instantiate a new `Base` layer that serves files from the
    /// supplied directory.
    pub fn new<P: Into<PathBuf>>(root: P, mime: MimeMap) -> Base {
        Base {
            root: root.into(),
            index: Index::Fallback(PathBuf::from("index.html")),
            mime,
        }
    }

    /// Builder pattern method to set directory indexing behavior.
    pub fn index(self, i: Index) -> Base {
        Base {
            root: self.root,
            index: i,
            mime: self.mime,
        }
    }

    pub fn webroot(&self) -> &Path {
        &self.root
    }

    /// If the request for a directory maps to an index _file_ (that is,
    /// if the `Base.index` is `Index::File(...)` or `Index::Fallback(...)`,
    /// then look for that file.
    ///
    /// `async` functions can't be recursive without boxing of their
    /// returned futures, therefor the `match` in `.find()` below can't
    /// just append the appropriate filename to the requested directory
    /// and then call itself again. (Or, at least, not without some
    /// hairy direct manipulation of futures.)
    async fn find_file(&self, p: PathBuf) -> eyre::Result<(PathBuf, Metadata)> {
        let md = metadata(&p).await?;
        if md.is_file() {
            event!(Level::DEBUG, "    found file: {}", p.display());
            Ok((p, md))
        } else {
            bail!("index target file is not file");
        }
    }

    /// Given the path from an HTTP request, attempt to map this to a
    /// path on the local filesystem.
    ///
    /// On success, return both the canonical form of the local path, and
    /// the `Metadata` for this resource; the caller will need both.
    async fn find(&self, p: &Path) -> eyre::Result<(PathBuf, Metadata)> {
        let mut path = self.root.clone();
        for segment in p.components().filter_map(|c| match c {
            Component::Normal(chunk) => Some(chunk),
            _ => None,
        }) {
            path.push(segment);
        }
        let p = path.canonicalize()?;
        event!(Level::DEBUG, "    canonical path: {}", path.display());
        let md = metadata(&p).await?;
        if md.is_file() {
            Ok((p, md))
        } else if md.is_dir() {
            match &self.index {
                Index::No => bail!("autoindexing disabled"),
                Index::Generate => Ok((p, md)),
                Index::File(ref f) => self.find_file(p.join(f)).await,
                Index::Fallback(ref f) => match self.find_file(p.join(f)).await {
                    Ok(pair) => Ok(pair),
                    Err(_) => Ok((p, md)),
                },
            }
        } else {
            Ok((p, md))
        }
    }

    /// Respond with a directory index.
    ///
    /// The HTML generated by `[dir::write_dir]` is pretty lean, so the
    /// directory would have to contain a _lot_ of entries in order for
    /// the size of the generated HTML to become a problem. As such, this
    /// function doesn't stream its output; it writes the entire thing to
    /// a buffer, then sends that. This way the `Content-Type` header can
    /// be set, and nothing has to be chunked.
    ///
    /// If this implementation ends up being problematic, it may change.
    async fn serve_dir(
        &self,
        url_path: &str,
        local_path: &Path,
        mod_info: Option<LastModified>,
    ) -> Result<SuResponse, SuErr> {
        let url_path = PathBuf::from(url_path);
        let mut buff: Vec<u8> = Vec::new();
        dir::write_dir(&mut buff, &url_path, local_path).await?;

        let mut r = Response::builder()
            .status(StatusCode::OK)
            .header(header::CONTENT_TYPE, HeaderValue::from_static("text/html"))
            .header(header::CONTENT_LENGTH, buff.len());

        if let Some(modified) = mod_info {
            r = modified.add_header(r);
        }

        let r = r.body(buff.into())?;
        Ok(r)
    }

    async fn serve_file(
        &self,
        local_path: &Path,
        md: &Metadata,
        maybe_modified: Option<LastModified>,
    ) -> Result<SuResponse, SuErr> {
        let f = File::open(local_path).await.map_err(|e| {
            SuErr::blank(
                StatusCode::INTERNAL_SERVER_ERROR,
                format!("error opening file {}: {}", local_path.display(), &e),
            )
        })?;

        let mime_type = match local_path.extension() {
            None => OCTET_STREAM.clone(),
            Some(ext) => {
                let mut ext = ext.to_os_string();
                ext.make_ascii_lowercase();
                self.mime.mime_type(&ext).clone()
            }
        };

        let mut r = Response::builder()
            .status(StatusCode::OK)
            .header(header::CONTENT_TYPE, mime_type)
            .header(header::CONTENT_LENGTH, md.len());

        if let Some(modified) = maybe_modified {
            r = modified.add_header(r);
        }

        let r = r.body(f.into())?;

        Ok(r)
    }

    async fn serve_range(
        &self,
        local_path: &Path,
        content_range: Range,
        maybe_modified: Option<LastModified>,
    ) -> Result<SuResponse, SuErr> {
        let mut f = File::open(local_path).await.map_err(|e| {
            SuErr::blank(
                StatusCode::INTERNAL_SERVER_ERROR,
                format!("error opening file {}: {}", local_path.display(), &e),
            )
        })?;

        let mime_type = match local_path.extension() {
            None => OCTET_STREAM.clone(),
            Some(ext) => {
                let mut ext = ext.to_os_string();
                ext.make_ascii_lowercase();
                self.mime.mime_type(&ext).clone()
            }
        };

        f.seek(SeekFrom::Start(content_range.start))
            .await
            .map_err(|e| {
                SuErr::blank(
                    StatusCode::INTERNAL_SERVER_ERROR,
                    format!(
                        "error seekng file {} to position {}: {}",
                        local_path.display(),
                        &content_range.start,
                        &e
                    ),
                )
            })?;

        let range_size = content_range.end - content_range.start;
        let range_header = content_range.to_header()?;

        let mut r = Response::builder()
            .status(StatusCode::PARTIAL_CONTENT)
            .header(header::CONTENT_TYPE, mime_type)
            .header(header::CONTENT_LENGTH, range_size)
            .header(header::CONTENT_RANGE, range_header);

        if let Some(modified) = maybe_modified {
            r = modified.add_header(r);
        }

        let r = r.body(Body::any(f.take(range_size)))?;
        Ok(r)
    }

    /**
    Attempt to supply a requested resource.

    The overall strategy here is to

      * map the request URI's path to a path on the filesystem
      * check whether the resource exists and is world-readable
      * determine the `Content-Length` from the file size (or the buffer size
        for generated directory indices) and guess at the `Content-Type`
        from the file extension
      * return the file opened for reading (or the cursor-wrapped buffer)
        back up the layer stack to be streamed out the TCP connection

    Eventually, this will also serve `Etag`s, and respond appropriately to
    `If-None-Match` and `If-Modified-Since` headers.
    */
    pub async fn handle(&self, r: SuRequest) -> Result<SuResponse, SuErr> {
        event!(
            Level::TRACE,
            "handle() called: {} {} {:?}",
            r.inner().method(),
            r.inner().uri(),
            r.inner().version()
        );

        match *(r.inner().method()) {
            Method::CONNECT | Method::DELETE | Method::PATCH | Method::POST | Method::PUT => {
                let r = Response::builder()
                    .status(StatusCode::METHOD_NOT_ALLOWED)
                    .header(header::ALLOW, HeaderValue::from_static("GET, TRACE"))
                    .body(Body::Empty)?;
                return Ok(r);
            }
            Method::TRACE => return respond_to_trace(r),
            _ => {}
        }

        let (p, md) = self
            .find(Path::new(r.inner().uri().path()))
            .await
            .map_err(|e| SuErr::blank(StatusCode::NOT_FOUND, e.to_string()))?;
        event!(Level::DEBUG, "    found path: {}, {:?}", p.display(), &md);
        permission_to_read(&md)?;

        let maybe_modified = match LastModified::try_from(&md) {
            Ok(lm) => Some(lm),
            Err(e) => {
                event!(
                    Level::WARN,
                    "unable to fetch modification time ({}): {}",
                    p.display(),
                    &e
                );
                None
            }
        };

        if let Some(modified) = maybe_modified {
            if let Some(response) = modified.check_if_modified(r.inner().headers()) {
                return Ok(response);
            }
        }

        if md.is_dir() {
            let request_path = r.inner().uri().path();
            if !matches!(request_path.as_bytes().last(), Some(b'/')) {
                return redirect_with_trailing_slash(request_path).await;
            }
            return self
                .serve_dir(r.inner().uri().path(), &p, maybe_modified)
                .await;
        }

        if let Some(range) = Range::try_from_headers(r.inner().headers(), &md)? {
            event!(Level::DEBUG, "range: {:?}", &range);
            if should_serve_range(maybe_modified, r.inner().headers()) {
                return self.serve_range(&p, range, maybe_modified).await;
            }
        }

        return self.serve_file(&p, &md, maybe_modified).await;
    }
}

impl Layer for Base {
    async fn call(&self, request: SuRequest) -> Option<SuResponse> {
        match self.handle(request).await {
            Ok(response) => Some(response),
            Err(e) => Some(e.to_response()),
        }
    }
}

/// Return a `FORBIDDEN` error if the given resource is not world-readable.
fn permission_to_read(md: &Metadata) -> Result<(), SuErr> {
    let mode = md.permissions().mode();
    if mode & READ_PERMISSION == 0 {
        Err(SuErr::blank(
            StatusCode::FORBIDDEN,
            format!("permissions are {:o}", &mode),
        ))
    } else {
        Ok(())
    }
}

async fn redirect_with_trailing_slash(url_path: &str) -> Result<SuResponse, SuErr> {
    let redirect_url = format!("{}/", url_path);
    let hval = HeaderValue::try_from(redirect_url).map_err(|e| {
        SuErr::blank(
            StatusCode::INTERNAL_SERVER_ERROR,
            format!("unable to turn url into header value {}/: {}", url_path, &e),
        )
    })?;

    let r = Response::builder()
        .status(StatusCode::MOVED_PERMANENTLY)
        .header(header::LOCATION, hval)
        .body(Body::Empty)?;
    Ok(r)
}

/**
Given that a request has requested a Range, should we serve just the
range (`true`) or the whole resource (`false`) ?
*/
fn should_serve_range(maybe_modified: Option<LastModified>, headers: &HeaderMap) -> bool {
    if let Some(hval) = headers.get(header::IF_RANGE) {
        if let Some(modified) = maybe_modified {
            if let Ok(range_modified) = LastModified::try_from(hval) {
                modified.0 < range_modified.0
            } else {
                false
            }
        } else {
            false
        }
    } else {
        true
    }
}
