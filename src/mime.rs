/*!
Dealing with MIME types.
*/
use std::{
    collections::BTreeMap,
    ffi::{OsStr, OsString},
};

use eyre::{eyre, WrapErr};
use http::HeaderValue;

pub static OCTET_STREAM: HeaderValue = HeaderValue::from_static("application/octet-stream");

static DEFAULT: &[(&str, &str)] = &[
    ("css", "text/css"),
    ("gz", "application/gzip"),
    ("gif", "image/gif"),
    ("htm", "text/html"),
    ("html", "text/html"),
    ("ico", "image/fnd.microsoft.icon"),
    ("jpeg", "image/jpeg"),
    ("jpg", "image/jpeg"),
    ("js", "text/javascript"),
    ("json", "application/json"),
    ("mp3", "audio/mpeg"),
    ("mp4", "audio/mp4"),
    ("mpeg", "video/mpeg"),
    ("png", "image/png"),
    ("pdf", "application/pdf"),
    ("svg", "image/svg+xml"),
    ("tar", "application/x-tar"),
    ("tif", "image/tiff"),
    ("tiff", "image/tiff"),
    ("txt", "text/plain"),
    ("wasm", "application/wasm"),
    ("wav", "audio/wav"),
    ("weba", "audio/webm"),
    ("webm", "video/webm"),
    ("webp", "image/webp"),
    ("woff", "font/woff"),
    ("woff2", "font/woff2"),
    ("xhtml", "application/xhtml+xml"),
    ("xml", "application/xml"),
    ("zip", "application/zip"),
    ("7z", "application/x-7z-compressed"),
];

#[derive(Debug)]
pub struct MimeMap {
    map: BTreeMap<OsString, HeaderValue>,
}

impl MimeMap {
    /// Instantiate an empty `MimeMap`.
    ///
    /// You probably want to use `MimeMap::default()` instead.
    pub fn empty() -> Self {
        Self {
            map: BTreeMap::default(),
        }
    }

    /// Add a particular mapping from file extension to MIME type.
    pub fn with<V>(self, k: &str, v: V) -> Self
    where
        HeaderValue: TryFrom<V>,
        <HeaderValue as TryFrom<V>>::Error: std::fmt::Debug,
    {
        let k = k.trim().to_lowercase().into();
        let v = HeaderValue::try_from(v).unwrap();
        let mut map = self.map;
        map.insert(k, v);
        Self { map }
    }

    pub fn try_insert<V>(&mut self, k: &str, v: V) -> eyre::Result<()>
    where
        HeaderValue: TryFrom<V>,
        <HeaderValue as TryFrom<V>>::Error: std::error::Error + Send + Sync + 'static,
    {
        let k = k.trim().to_lowercase().into();
        let v = HeaderValue::try_from(v).wrap_err("converting mime type to header value")?;
        self.map.insert(k, v);

        Ok(())
    }

    /// Add all the entries from `other` to `self`.
    pub fn absorb(&mut self, other: Self) {
        for (k, v) in other.map.into_iter() {
            self.map.insert(k, v);
        }
    }

    pub fn mime_type<K: AsRef<OsStr>>(&self, k: K) -> &HeaderValue {
        self.map.get(k.as_ref()).unwrap_or(&OCTET_STREAM)
    }
}

impl Default for MimeMap {
    /// Instantiate a `MimeMap` prepopulated with common MIME types.
    fn default() -> Self {
        use std::str::FromStr;
        let map: BTreeMap<OsString, HeaderValue> = DEFAULT
            .iter()
            .map(|(k, v)| {
                (
                    OsString::from_str(k).unwrap(),
                    HeaderValue::from_str(v).unwrap(),
                )
            })
            .collect();

        Self { map }
    }
}
