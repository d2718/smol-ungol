/*!
Miscellaneous types of `Layer`s
*/
use http::{
    header::{HeaderName, HeaderValue},
    Method, Response, StatusCode,
};
use smol::{io::AsyncWriteExt, lock::Mutex};

use super::Layer;
use crate::{body::Body, errors::SuErr, req::SuRequest, SuResponse};

/// A layer that adds a fixed set of headers to every outgoing response.
pub struct AddHeaders<T> {
    wrapped: T,
    headers: Vec<(HeaderName, HeaderValue)>,
}

impl<T> AddHeaders<T> {
    /// Instantiate a new `AddHeaders` layer, wrapping an inner layer.
    pub fn new(wrapped: T) -> Self {
        Self {
            wrapped,
            headers: Vec::new(),
        }
    }

    /// Builder-pattern method to specify a header to add.
    pub fn with(self, name: HeaderName, val: HeaderValue) -> Self {
        let mut headers = self.headers;
        headers.push((name, val));
        Self {
            wrapped: self.wrapped,
            headers,
        }
    }

    // Do the thing.
    fn replace_headers(&self, mut resp: SuResponse) -> SuResponse {
        let headers = resp.headers_mut();
        for (name, val) in self.headers.iter() {
            headers.append(name, val.clone());
        }
        resp
    }
}

impl<T> Layer for AddHeaders<T>
where
    T: Layer,
{
    async fn call(&self, req: SuRequest) -> Option<SuResponse> {
        self.wrapped
            .call(req)
            .await
            .map(|r| self.replace_headers(r))
    }
}

pub struct Logging<T, W> {
    wrapped: T,
    writer: Mutex<W>,
}

/// A layer for logging requests.
impl<T, W> Logging<T, W> {
    pub fn new(wrapped: T, writer: W) -> Self {
        Self {
            wrapped,
            writer: Mutex::new(writer),
        }
    }
}

// If we want to consider a different log time format at some point.
// static DATE_FMT: &[FormatItem] = format_description!("[]");

const LOG_LINE_BUFFER_SIZE: usize = 128;

impl<T, W> Layer for Logging<T, W>
where
    T: Layer,
    W: AsyncWriteExt + Unpin,
{
    async fn call(&self, req: SuRequest) -> Option<SuResponse> {
        use std::io::Write;
        use time::{format_description::well_known::Rfc3339, OffsetDateTime};

        let mut buff: Vec<u8> = Vec::with_capacity(LOG_LINE_BUFFER_SIZE);
        OffsetDateTime::now_utc()
            .format_into(&mut buff, &Rfc3339)
            .unwrap();
        write!(
            &mut buff,
            " {} {} {}",
            &req.addr,
            req.inner().method().as_str(),
            req.inner().uri()
        )
        .unwrap();

        let response = self.wrapped.call(req).await;

        // All the `write!` results in this match chunk get unwrapped;
        // this is probably fine because if writing to an im-memory
        // `String` fails, we're probably in trouble and are going to
        // die anyway.
        match &response {
            Some(res) => match res.status().canonical_reason() {
                Some(reason) => {
                    writeln!(&mut buff, " {} ({})", res.status().as_str(), reason).unwrap()
                }
                None => writeln!(&mut buff, " {}", res.status().as_str()).unwrap(),
            },
            None => writeln!(&mut buff, " DROPPED").unwrap(),
        }

        if let Err(e) = self.writer.lock().await.write_all(&buff).await {
            Some(
                SuErr::blank(
                    StatusCode::INTERNAL_SERVER_ERROR,
                    format!("error writing to request log file: {}", &e),
                )
                .to_response(),
            )
        } else {
            response
        }
    }
}

/// Strips the body out of HEAD requests.
pub struct Decapitator<L> {
    inner: L,
}

impl<L> Decapitator<L> {
    pub fn new(inner: L) -> Decapitator<L> {
        Decapitator { inner }
    }
}

impl<L: Layer> Layer for Decapitator<L> {
    async fn call(&self, req: SuRequest) -> Option<SuResponse> {
        match req.inner().method() {
            &Method::HEAD => match self.inner.call(req).await {
                Some(response) => {
                    let (p, _) = response.into_parts();
                    Some(Response::from_parts(p, Body::Empty))
                }
                None => None,
            },
            _ => self.inner.call(req).await,
        }
    }
}
