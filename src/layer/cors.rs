/*!
Configurable CORS layer.

In the future, all the response parts will be constructed once, when the
server configuration is read, and just cloned on response.
*/
use http::{header, HeaderValue, Method, Response};
use tracing::{event, Level};

use crate::{body::Body, errors::SuErr, layer::Layer, SuRequest, SuResponse};

static ALL: HeaderValue = HeaderValue::from_static("*");
static METHODS: HeaderValue = HeaderValue::from_static("GET, HEAD, OPTIONS, TRACE");

pub enum AllowList {
    None,
    All,
    List(HeaderValue),
}

pub struct Cors<L> {
    inner: L,
    origins: AllowList,
    headers: AllowList,
}

impl<L: Layer> Layer for Cors<L> {
    async fn call(&self, request: SuRequest) -> Option<SuResponse> {
        event!(Level::TRACE, "Cors::call() called");

        if request.inner().method() != Method::OPTIONS {
            return self.inner.call(request).await;
        }

        let mut response = Response::builder().status(http::StatusCode::OK);

        match self.origins {
            AllowList::All => {
                response = response
                    .header(header::ACCESS_CONTROL_ALLOW_ORIGIN, ALL.clone())
                    .header(header::ACCESS_CONTROL_ALLOW_METHODS, METHODS.clone())
                    .header(header::ACCESS_CONTROL_ALLOW_HEADERS, ALL.clone());
            }
            AllowList::List(ref val) => {
                response = response
                    .header(header::ACCESS_CONTROL_ALLOW_ORIGIN, val.clone())
                    .header(header::ACCESS_CONTROL_ALLOW_METHODS, METHODS.clone())
                    .header(header::ACCESS_CONTROL_ALLOW_HEADERS, ALL.clone())
            }
            _ => {}
        }

        if !matches!(self.origins, AllowList::None) {
            match self.headers {
                AllowList::All => {
                    response = response.header(header::ACCESS_CONTROL_EXPOSE_HEADERS, ALL.clone())
                }
                AllowList::List(ref val) => {
                    response = response.header(header::ACCESS_CONTROL_EXPOSE_HEADERS, val.clone());
                }
                _ => {}
            }
        }

        match response.body(Body::Empty) {
            Ok(response) => Some(response),
            Err(e) => Some(SuErr::from(e).to_response()),
        }
    }
}
