/*!
Compression layer.
*/
use std::{
    collections::BTreeSet,
    pin::Pin,
    str::FromStr,
    task::{Context, Poll},
};

use async_compression::futures::bufread::{BrotliEncoder, DeflateEncoder, GzipEncoder};
use http::{
    header::{self, HeaderValue},
    Response,
};
use pin_project::pin_project;
use smol::io::{AsyncBufRead, AsyncRead, BufReader, Cursor};

use crate::{
    body::Body, config::CmpCfg, errors::SuErr, layer::Layer, request_header, SuRequest, SuResponse,
};

static ALGO_PARSE_ERR: &str = "invalid algorithm; valid values are gzip, deflate, br, identity";
static ACCEPT_BYTES: HeaderValue = HeaderValue::from_static("bytes");

// MIME types to NOT compress, regardless of incoming Accept-Encoding.
const DEFAULT_IGNORES: &[&str] = &[
    "application/gzip",
    "application/x-7z-compressed",
    "application/zip",
    "audio/mp4",
    "audio/mpeg",
    "audio/webm",
    "image/gif",
    "image/jpeg",
    "image/png",
    "image/webp",
    "video/mpeg",
    "video/webm",
];

fn default_ignores() -> BTreeSet<HeaderValue> {
    DEFAULT_IGNORES
        .iter()
        .map(|&s| HeaderValue::from_static(s))
        .collect()
}

// The compression algorithm to use.
#[derive(Clone, Copy, Debug, Default, PartialEq, Eq)]
pub enum Algo {
    Gzip,
    Deflate,
    Br,
    // No compression
    #[default]
    Pass,
}

impl FromStr for Algo {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let s = s.trim_start();

        match s.get(..2) {
            None => return Err(ALGO_PARSE_ERR),
            Some("br") => return Ok(Algo::Br),
            _ => {}
        }

        match s.get(..4) {
            None => return Err(ALGO_PARSE_ERR),
            Some("gzip") => return Ok(Algo::Gzip),
            _ => {}
        }

        match s.get(..7) {
            None => return Err(ALGO_PARSE_ERR),
            Some("deflate") => return Ok(Algo::Deflate),
            _ => {}
        }

        if let Some("identity") = s.get(..8) {
            Ok(Algo::Pass)
        } else {
            Err(ALGO_PARSE_ERR)
        }
    }
}

impl From<Algo> for HeaderValue {
    fn from(a: Algo) -> HeaderValue {
        match a {
            Algo::Gzip => HeaderValue::from_static("gzip"),
            Algo::Deflate => HeaderValue::from_static("deflate"),
            Algo::Br => HeaderValue::from_static("br"),
            Algo::Pass => HeaderValue::from_static("identity"),
        }
    }
}

#[derive(Debug)]
#[pin_project(project = EncProj)]
enum Encoder<R> {
    Gzip(#[pin] GzipEncoder<R>),
    Deflate(#[pin] DeflateEncoder<R>),
    Br(#[pin] Box<BrotliEncoder<R>>),
    Pass(#[pin] R),
}

impl<R: AsyncBufRead> Encoder<R> {
    fn new(a: Algo, r: R) -> Encoder<R> {
        match a {
            Algo::Gzip => Encoder::Gzip(GzipEncoder::new(r)),
            Algo::Deflate => Encoder::Deflate(DeflateEncoder::new(r)),
            Algo::Br => Encoder::Br(Box::new(BrotliEncoder::new(r))),
            Algo::Pass => Encoder::Pass(r),
        }
    }
}

impl<R: AsyncBufRead + Unpin> AsyncRead for Encoder<R> {
    fn poll_read(
        self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buff: &mut [u8],
    ) -> Poll<Result<usize, smol::io::Error>> {
        match self.project() {
            EncProj::Gzip(enc) => enc.poll_read(cx, buff),
            EncProj::Deflate(enc) => enc.poll_read(cx, buff),
            EncProj::Br(enc) => enc.poll_read(cx, buff),
            EncProj::Pass(r) => r.poll_read(cx, buff),
        }
    }
}

pub struct Compression<L> {
    inner: L,
    types: Vec<Algo>,
    min_size: u64,
    ignores: BTreeSet<HeaderValue>,
}

impl<L> Compression<L> {
    pub fn new(inner: L, cfg: CmpCfg) -> Compression<L> {
        let mut ignores: BTreeSet<HeaderValue> = if cfg.no_default_ignores {
            BTreeSet::default()
        } else {
            default_ignores()
        };

        for hval in cfg.ignore.into_iter() {
            ignores.insert(hval);
        }

        Compression {
            inner,
            types: cfg.types,
            min_size: cfg.min_size,
            ignores,
        }
    }

    fn get_compression_type(&self, r: &SuRequest) -> Option<Algo> {
        let text = request_header(r, header::ACCEPT_ENCODING)?;
        for chunk in text.trim().split_ascii_whitespace() {
            if let Ok(a) = Algo::from_str(chunk) {
                if self.types.contains(&a) {
                    return Some(a);
                }
            }
        }

        None
    }

    async fn compress(&self, algo: Algo, response: SuResponse) -> Result<SuResponse, SuErr> {
        let (mut parts, body) = response.into_parts();

        let body = match body {
            Body::Empty => return Ok(Response::from_parts(parts, body)),
            Body::Static(bytes) => Body::any(Encoder::new(algo, bytes)),
            Body::Vec(v) => Body::any(Encoder::new(algo, BufReader::new(Cursor::new(v)))),
            Body::File(f) => Body::any(Encoder::new(algo, BufReader::new(f))),
            Body::Boxed(b) => Body::any(Encoder::new(algo, BufReader::new(b))),
        };

        let _ = parts.headers.remove(header::CONTENT_LENGTH);
        parts.headers.insert(header::CONTENT_ENCODING, algo.into());
        parts
            .headers
            .insert(header::VARY, HeaderValue::from_static("Accept-Encoding"));

        Ok(Response::from_parts(parts, body))
    }
}

impl<L> Layer for Compression<L>
where
    L: Layer,
{
    async fn call(&self, req: SuRequest) -> Option<SuResponse> {
        let mut algo = Algo::Pass;
        if let Some(a) = self.get_compression_type(&req) {
            algo = a;
        }

        let mut response = self.inner.call(req).await?;

        if response.headers().get(header::CONTENT_RANGE).is_some() {
            return Some(response);
        }

        if algo == Algo::Pass {
            response
                .headers_mut()
                .insert(header::ACCEPT_RANGES, ACCEPT_BYTES.clone());
            return Some(response);
        }

        if let Some(t) = response.headers().get(header::CONTENT_TYPE) {
            if self.ignores.contains(t) {
                response
                    .headers_mut()
                    .insert(header::ACCEPT_RANGES, ACCEPT_BYTES.clone());
                return Some(response);
            }
        }

        if let Some(v) = response.headers().get(header::CONTENT_LENGTH) {
            if let Ok(v) = v.to_str() {
                if let Ok(n) = v.parse::<u64>() {
                    if n < self.min_size {
                        return Some(response);
                    }
                }
            }
        }

        match self.compress(algo, response).await {
            Ok(response) => Some(response),
            Err(e) => Some(e.to_response()),
        }
    }
}
