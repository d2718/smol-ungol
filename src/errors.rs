/*!
The error type used throughout when more structured error reporting
is necessary than just "here, toss this up the stack".
*/
use crate::SuResponse;
use http::{header, HeaderValue, Response, StatusCode};
use std::borrow::Cow;

use crate::body::Body;

static TEXT_CONTENT_TYPE: HeaderValue = HeaderValue::from_static("text/plain");

#[derive(Debug)]
pub struct SuErr {
    status: StatusCode,
    log: Cow<'static, str>,
    client_msg: Option<String>,
}

impl SuErr {
    pub fn status(&self) -> StatusCode {
        self.status
    }

    pub fn log(&self) -> &str {
        self.log.as_ref()
    }

    pub fn blank<L>(status: StatusCode, log: L) -> Self
    where
        L: Into<Cow<'static, str>>,
    {
        Self {
            status,
            log: log.into(),
            client_msg: None,
        }
    }

    pub fn message<L>(status: StatusCode, log: L, msg: String) -> Self
    where
        L: Into<Cow<'static, str>>,
    {
        Self {
            status,
            log: log.into(),
            client_msg: Some(msg),
        }
    }

    pub fn to_response(self) -> SuResponse {
        match self.client_msg {
            None => Response::builder()
                .status(self.status)
                .body(Body::Empty)
                .unwrap(),
            Some(msg) => Response::builder()
                .status(self.status)
                .header(header::CONTENT_TYPE, &TEXT_CONTENT_TYPE)
                .header(header::CONTENT_LENGTH, msg.len())
                .body(msg.into_bytes().into())
                .unwrap(),
        }
    }
}

impl std::fmt::Display for SuErr {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Error {}: {:?} ({})",
            &self.status, &self.client_msg, &self.log
        )
    }
}

impl std::error::Error for SuErr {}

impl From<core::fmt::Error> for SuErr {
    fn from(e: core::fmt::Error) -> Self {
        SuErr::blank(
            StatusCode::INTERNAL_SERVER_ERROR,
            format!("read error: {}", &e),
        )
    }
}

impl From<std::io::Error> for SuErr {
    fn from(e: std::io::Error) -> Self {
        SuErr::blank(
            StatusCode::INTERNAL_SERVER_ERROR,
            format!("io::read error: {}", &e),
        )
    }
}

impl From<http::Error> for SuErr {
    fn from(e: http::Error) -> Self {
        SuErr::blank(
            StatusCode::INTERNAL_SERVER_ERROR,
            format!("http::Error: {}", &e),
        )
    }
}
