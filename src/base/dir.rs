/*!
Separate module for writing directory indices.
*/
use std::{
    io::Write,
    path::{Path, PathBuf},
};

use eyre::{bail, WrapErr};
use http::StatusCode;
use smol::{
    fs::{read_dir, DirEntry},
    stream::StreamExt,
};
use time::{format_description::FormatItem, macros::format_description, OffsetDateTime};
use tracing::{event, Level};

use crate::errors::SuErr;

static TIME_FMT: &[FormatItem] =
    format_description!("[year]-[month]-[day] [hour repr:24]-[minute]-[second]");

static HEAD: &str = include_str!("head.html");
static MIDD: &str = include_str!("mid.html");
static FOOT: &str = include_str!("foot.html");

/**
Write a table row representing a single directory entry to the provided
buffer.

`base` should be a pathbuf containing the URL path (not the local filesystem
path) of the directory whose listing is currently being served. The
entry's name will be pushed onto it temporarily in order to write the
correct link URL path (and then popped off again before returning).
*/
async fn write_metadata(w: &mut Vec<u8>, base: &mut PathBuf, de: &DirEntry) -> eyre::Result<()> {
    let md = de.metadata().await?;

    // If it's not a directory or a regular file, skip it.
    if !(md.is_dir() || md.is_file()) {
        return Ok(());
    }

    let path = de.path();
    let file_name = match path.file_name() {
        None => return Ok(()),
        Some(name) => name,
    };
    let utf8_name = file_name.to_string_lossy();

    base.push(file_name);
    let res = base.to_str();
    let utf8_path = match res {
        None => bail!("paths must be valid UTF-8"),
        Some(p) => p,
    };

    let mod_time = OffsetDateTime::from(md.modified().wrap_err("unable to get modified time")?);

    if md.is_file() {
        write!(w, "<tr>\n  <td>{}</td>\n  <td>", md.len())?;
    } else {
        write!(w, "<tr>\n  <td></td>\n  <td>")?;
    }
    mod_time.format_into(w, TIME_FMT)?;
    write!(
        w,
        "</td>\n  <td><a href=\"{}\">{}</a></td></tr>",
        &utf8_path, &utf8_name
    )?;

    Ok(())
}

/**
Write HTML representing the given directory listing to the provided buffer.

This function requires both the local fielsystem path of the directory
(to correctly find and enumerate its entries) and the URL path that maps
to the directory (in order to correctly generate link tags).
*/
pub async fn write_dir(w: &mut Vec<u8>, url_path: &Path, local_path: &Path) -> Result<(), SuErr> {
    let mut entry_iter = read_dir(local_path).await.map_err(|e| {
        SuErr::blank(
            StatusCode::INTERNAL_SERVER_ERROR,
            format!(
                "unable to call read_dir() on {}: {}",
                local_path.display(),
                &e
            ),
        )
    })?;

    w.extend_from_slice(HEAD.as_bytes());
    write!(w, "{}", url_path.display()).map_err(|e| {
        SuErr::blank(
            StatusCode::INTERNAL_SERVER_ERROR,
            format!(
                "unable to write directory name ({})to output buffer: {}",
                local_path.display(),
                &e
            ),
        )
    })?;
    w.extend_from_slice(MIDD.as_bytes());

    // We're going to reuse this same PathBuf for each DirEntry.
    let mut base = PathBuf::from(url_path);

    event!(
        Level::DEBUG,
        "url_path: {}, local_path: {}, base: {}",
        url_path.display(),
        local_path.display(),
        base.display()
    );

    // We need to keep track of the length of the output buffer, because
    // if there's an error writing out a particular entry, we need to
    // back up to a point before we started writing that entry. This
    // gets updated at the end of each successful write.
    let mut out_pos = w.len();

    while let Some(maybe_entry) = entry_iter.next().await {
        let entry = match maybe_entry {
            Ok(entry) => entry,
            Err(e) => {
                event!(
                    Level::WARN,
                    "error reading directory entry in {}: {}",
                    local_path.display(),
                    &e
                );
                continue;
            }
        };

        let res = write_metadata(w, &mut base, &entry).await;
        base.pop();
        if let Err(e) = res {
            event!(
                Level::WARN,
                "error writing metadata for directory entry in {}: {}",
                local_path.display(),
                &e
            );
            // Our write was unsuccessful; discard everything after the
            // last successful write.
            w.resize(out_pos, 0u8);
        } else {
            // Our write was successful; update the length of the buffer.
            out_pos = w.len();
        }
    }

    w.extend_from_slice(FOOT.as_bytes());
    Ok(())
}
