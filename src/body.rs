/*!
The response body type.

The response body type is a unification of the various sources of bytes
that can make up the body of a response.
*/
use std::fmt::{Debug, Formatter};

use smol::{fs::File, io::AsyncRead};

pub enum Body {
    Empty,
    Static(&'static [u8]),
    Vec(Vec<u8>),
    File(File),
    Boxed(Box<dyn AsyncRead + Unpin>),
}

impl Debug for Body {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match *self {
            Body::Empty => write!(f, "Body::Empty"),
            Body::Static(s) => write!(f, "Body::Static({:?})", &String::from_utf8_lossy(s)),
            Body::Vec(ref v) => write!(f, "Body::Vec({} bytes)", v.len()),
            Body::File(ref fref) => smol::block_on(async {
                match fref.metadata().await {
                    Ok(m) => write!(f, "Body::File({} bytes)", &m.len()),
                    Err(_) => write!(f, "Body::File( [can't read length] )"),
                }
            }),
            Body::Boxed(_) => write!(f, "Body::Boxed(???)"),
        }
    }
}

impl From<&'static [u8]> for Body {
    fn from(bytes: &'static [u8]) -> Body {
        Body::Static(bytes)
    }
}

impl From<&'static str> for Body {
    fn from(s: &'static str) -> Body {
        Body::Static(s.as_bytes())
    }
}

impl From<Vec<u8>> for Body {
    fn from(v: Vec<u8>) -> Body {
        Body::Vec(v)
    }
}

impl From<File> for Body {
    fn from(f: File) -> Body {
        Body::File(f)
    }
}

impl Body {
    pub fn any<R: AsyncRead + Unpin + 'static>(r: R) -> Body {
        Body::Boxed(Box::new(r))
    }
}
